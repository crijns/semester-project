"""
Calculate the Poincaré-Steklov operator for the Laplacian
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as ls
import scipy.linalg as la
from scipy.stats.qmc import LatinHypercube
from utils.helper_functions import fd_laplacian, flattened_ij_meshgrid, plot_and_pad
from tqdm import tqdm
from tensorflow.keras.models import load_model

# Domain and decomposition specification
Lx = 1
Ly = 1

# Percentage of first domain
percentage = 0.6

# Discretisation
# Nx, Ny number of intervals
Nx = Ny = 40
hx = Lx / Nx
hy = Ly / Ny

(X, Y), (X_flat, Y_flat) = flattened_ij_meshgrid((0, Lx, Nx + 1),
                                                 (0, Ly, Ny + 1))

(Xint, Yint), (Xint_flat, Yint_flat) = flattened_ij_meshgrid((hx, Lx - hx, Nx - 1),
                                                             (hy, Ly - hy, Ny - 1))

# Separation of subdomains
Nx1 = round(percentage * Nx)
Nx2 = Nx - Nx1

print(f"Domain has {Nx}x{Ny} intervals")
print(f"Subdomains have, for 1: {Nx1}x{Ny} and for 2: {Nx2}x{Ny} intervals")

# First domain
(X1, Y1), (X1_flat, Y1_flat) = flattened_ij_meshgrid((0, percentage * Lx, Nx1 + 1),
                                                     (0, Ly, Ny + 2))

# Second domain
(X2, Y2), (X2_flat, Y2_flat) = flattened_ij_meshgrid((percentage * Lx, (1 - percentage) * Lx, Nx2 + 1),
                                                     (0, Ly, Ny + 2))

# Source term and exact solution for Laplace equation
n = 3
m = 2
f = lambda x, y: ((n * np.pi / Lx) ** 2 + (m * np.pi / Ly) ** 2) \
                 * np.sin(n * np.pi / Lx * x) \
                 * np.sin(m * np.pi / Ly * y)
uex = lambda x, y: np.sin(n * np.pi / Lx * x) * np.sin(m * np.pi / Ly * y)

# Global Discretisation

# Matrices
AX = fd_laplacian(Nx - 1, hx)
AY = fd_laplacian(Ny - 1, hy)
A = sp.kron(AX, sp.eye(Ny - 1)) + sp.kron(sp.eye(Nx - 1), AY)

# RHS
rhs = f(Xint_flat, Yint_flat)

# Solving solution -- Finite difference solving
u = ls.spsolve(A, rhs)

# View solution
plt.figure(1)
plot_and_pad(u.reshape((Nx-1, Ny-1)), [0, 1, 0, 1], ((1, 1), (1, 1)))
plt.title("Global Solution")
# plt.show()

# Local Discretisation

inveps = 1e10

# Local matrices

# Problem 1
A1X = fd_laplacian(Nx1, hx)
A1Y = fd_laplacian(Ny - 1, hy)
A1 = sp.kron(A1X, sp.eye(Ny - 1)) + sp.kron(sp.eye(Nx1), A1Y)

# Problem 2
A2X = fd_laplacian(Nx2, hx)
A2Y = fd_laplacian(Ny - 1, hy)
A2 = sp.kron(A2X, sp.eye(Ny - 1)) + sp.kron(sp.eye(Nx2), A2Y)

# Correct for Dirichlet BC
A1[-Ny + 1:, -Ny + 1:] = A1[-Ny + 1:, -Ny + 1:] / 2 + inveps * sp.eye(Ny - 1)

# Correct for Neumann BC
A2[:Ny, :Ny] /= 2

# Coupling matrices
C1X = sp.dok_matrix((Nx1, Nx2))
C1X[-1, 0] = inveps
C1 = sp.kron(C1X, sp.eye(Ny - 1))

C2X = sp.dok_matrix((Nx2, Nx1))
C2X[0, -1] = -1 / hx
C2X[0, -2] = 1 / hx
# Force CSC sparse format for slicing, as CSR format does not support slicing
C2 = sp.kron(C2X, sp.eye(Ny - 1), format="csc")

# Right hand sides
rhs = np.hstack((f(Xint_flat[:(Ny - 1) * Nx1], Yint_flat[:(Ny - 1) * Nx1]),
                 f(Xint_flat[(Ny - 1) * (Nx1 - 1):], Yint_flat[(Ny - 1) * (Nx1 - 1):])
                 ))[..., np.newaxis]

# Correct for Dirichlet
rhs[(Ny - 1) * (Nx1 - 1):(Ny - 1) * Nx1] /= 2
# Correct for Neumann
rhs[(Ny - 1) * Nx1:(Ny - 1) * (Nx1 + 1)] /= 2

# Domain Decomposition
maxiter = 400

dirichlet_values = np.zeros(Ny - 1)

for i in tqdm(range(maxiter)):
    u1_guess = ls.spsolve(A1, rhs[:A1.shape[0]] + C1 @ np.hstack((dirichlet_values, np.zeros((Nx2 - 1) * (Ny - 1))))[
        ..., np.newaxis])
    neumann_values = C2[:Ny - 1, :] @ u1_guess
    u2_guess = ls.spsolve(A2, rhs[A1.shape[0]:] + np.hstack((1 / hx * neumann_values, np.zeros((Nx2 - 1) * (Ny - 1))))[
        ..., np.newaxis])
    dirichlet_values = u2_guess[:Ny - 1]

u_full = np.vstack((u1_guess.reshape(Nx1, Ny - 1), u2_guess.reshape(Nx2, Ny - 1)[1:, :]))
plt.figure(2)
plot_and_pad(u_full, [0, 1, 0, 1], ((1, 1), (1, 1)))
plt.title("Domain Decomposition")
# plt.show()

# Poincaré-Steklov operator

# Linear part of operator
S = np.zeros((Ny - 1, Ny - 1))
for j in range(Ny - 1):
    e_basis = np.zeros((Ny - 1, 1))
    e_basis[j] = 1
    e_basis_extended = np.vstack((e_basis, np.zeros(((Nx2 - 1) * (Ny - 1), 1))))
    solution = ls.spsolve(A2, 0 + 1 / hx * e_basis_extended)
    S[:, j] = solution[:Ny - 1]

# Affine part
solution = ls.spsolve(A2, rhs[A1.shape[0]:])
S0 = solution[:Ny - 1]

# Solve using operator
dirichlet_values = np.zeros(Ny - 1)

for i in tqdm(range(maxiter)):
    u1_guess = ls.spsolve(A1, rhs[:A1.shape[0]] + C1 @ np.hstack((dirichlet_values, np.zeros((Nx2 - 1) * (Ny - 1))))[
        ..., np.newaxis])
    neumann_values = C2[:Ny - 1, :] @ u1_guess
    dirichlet_values = S0 + S @ neumann_values

u_full = np.vstack((u1_guess.reshape(Nx1, Ny - 1), u2_guess.reshape(Nx2, Ny - 1)[1:, :]))
plt.figure(3)
plt.title("Operator Solution")
plot_and_pad(u_full, [0, 1, 0, 1], ((1, 1), (1, 1)))
# plt.show()

# Reduced Basis
n_basis = 5
fourier_basis = np.sin(
    (np.arange(1, n_basis + 1).reshape(1, -1) * np.pi / Ly * np.linspace(hy, Ly - hy, Ny - 1).reshape(-1, 1)))

system_response_solution = np.zeros((A2.shape[0], n_basis))
system_response_boundary = np.zeros((Ny - 1, n_basis))

for j in range(n_basis):
    boundary_vector = fourier_basis[:, j]
    boundary_vector_extended = np.vstack((boundary_vector[..., np.newaxis], np.zeros(((Nx2 - 1) * (Ny - 1), 1))))
    system_response_solution[:, j] = ls.spsolve(A2, rhs[A1.shape[0]:] + 1 / hx * boundary_vector_extended)
    system_response_boundary[:, j] = system_response_solution[:Ny - 1, j]

# Apply SVD on whole system
n_latent = 5
U2, S, Vh = la.svd(system_response_solution, full_matrices=False)

# Plot singular values
plt.figure(4)
plt.semilogy(S/S[0])
plt.title("Singular Values of full system response")
# plt.show()

# Plot singular vectors
plt.figure(5)
for i in range(n_latent):
    plt.subplot(1, n_latent, i+1)
    plot_and_pad(U2[:, i].reshape(Nx2, Ny-1), [0.6, 1, 0, 1], ((0, 1), (1, 1)))
plt.title("Singular Vectors of full system response")
# plt.show()

# Reduced Matrices
A2R = U2.T @ A2 @ U2

# Apply SVD only on boundary
U2B, SB, VhB = la.svd(system_response_boundary, full_matrices=False)
U2B = U2B[:, :n_latent]

# Plot singular values
plt.figure(6)
plt.title("Singular Values of boundary response")
plt.semilogy(SB/SB[0])
# plt.show()

# Plot singular vectors
plt.figure(7)
plt.title("Singular Vectors of boundary response")
plt.plot(U2B)
# plt.show()

# Reduced matrices for Poincaré-Steklov
Sred = S * fourier_basis

# Precompute pseudoinverse
pinv_fourier_basis = la.pinv(fourier_basis)

# Make Neural Network data using Fourier basis
n_data_points = 200
min_val = -100
max_val = 100
# Construct Latin Hypercube Sampler
sampler = LatinHypercube(n_basis)
coefficients = min_val + (sampler.random(n_data_points)*(max_val-min_val)).T
boundaries = fourier_basis @ coefficients

output_system_response = np.zeros((A2.shape[0], n_data_points))
output_system_response_boundary = np.zeros((Ny-1, n_data_points))

for j in tqdm(range(n_data_points)):
    boundary_vector = boundaries[:, j]
    boundary_vector_extended = np.vstack((boundary_vector[..., np.newaxis], np.zeros(((Nx2 - 1) * (Ny - 1), 1))))
    output_system_response[:, j] = ls.spsolve(A2, rhs[A1.shape[0]:] + 1 / hx * boundary_vector_extended)
    output_system_response_boundary[:, j] = output_system_response[:Ny - 1, j]

np.savez("neural_network_data.npz", input=coefficients, output=U2.T @ output_system_response,
         output_bd=U2B.T @ output_system_response_boundary)

input("Press enter to resume after training")

# Load network
model = load_model("trained_neural_network")

# Solve using operator
dirichlet_values = np.zeros(Ny - 1)

for i in tqdm(range(maxiter)):
    u1_guess = ls.spsolve(A1, rhs[:A1.shape[0]] + C1 @ np.hstack((dirichlet_values, np.zeros((Nx2 - 1) * (Ny - 1))))[
        ..., np.newaxis])
    neumann_values = C2[:Ny - 1, :] @ u1_guess[..., np.newaxis]
    dirichlet_values = U2[:Ny-1, :] @ model.predict((pinv_fourier_basis @ neumann_values).T).reshape(-1)

u2_guess = ls.spsolve(A2, rhs[A1.shape[0]:] + (C2 @ u1_guess)[..., np.newaxis])

u_full = np.vstack((u1_guess.reshape(Nx1, Ny - 1), u2_guess.reshape(Nx2, Ny - 1)[1:, :]))
plt.figure(8)
plot_and_pad(u_full, [0, 1, 0, 1], ((1, 1), (1, 1)))
plt.title("Neural Network solution")
plt.show()
