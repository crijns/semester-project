import numpy as np
from demonstration.neural_network import build_network
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from functools import partial
import matplotlib.pyplot as plt

data = np.load("neural_network_data.npz")
input_data = data['input']
output = data['output']
output_bd = data['output_bd']

lambda_reg = 0.0001
n_epochs = 1000

model = build_network(output.shape[0], 'relu', 10, 4, partial(l2, lambda_reg))
model.compile(optimizer=RMSprop(learning_rate=0.001, rho=0.9), loss='mean_squared_error', metrics=['mean_squared_error'])

history = model.fit(input_data.T, output.T, epochs=n_epochs, validation_split=0.2)

model.summary()

plt.semilogy(history.history['mean_squared_error'], label='train')
plt.semilogy(history.history['val_mean_squared_error'], label='validation')
plt.title("Model Accuracy")
plt.ylabel("loss")
plt.xlabel("epoch")
plt.legend()
plt.show()

model.save("trained_neural_network")

