import matplotlib.pyplot as plt
from math import sqrt


def setup_plots(scale=1, square=False, width_scale=1):
    # Matplotlib RC
    fig_width_pt = 438.81264/scale  # Get this from LaTeX using \showthe\columnwidth
    inches_per_pt = 1.0 / 72.27  # Convert pt to inches
    golden_mean = (sqrt(5) - 1.0) / 2.0  # Aesthetic ratio
    fig_width = fig_width_pt * inches_per_pt  # width in inches
    fig_height = fig_width * golden_mean  # height in inches
    if square:
        fig_size = [fig_height, fig_height]
    else:
        fig_size = [fig_width*width_scale, fig_height]
    params = {'font.size': 8/scale,
              'text.usetex': True,
              'text.latex.preamble': r"""
    \usepackage{amsmath}
    """,
              'figure.figsize': fig_size}
    plt.rcParams.update(params)