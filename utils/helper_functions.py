"""
Helper functions for the construction of matrices
"""
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sp
import scipy.sparse.linalg as ls
import functools
import weakref
from utils.plot_settings import setup_plots


def fd_laplacian(n, h):
    e = np.ones(n)
    return 1 / h ** 2 * sp.diags([-e[1:], 2 * e, -e[1:]], [-1, 0, 1])


def flattened_ij_meshgrid(range1, range2):
    x, y = np.meshgrid(
        np.linspace(*range1),
        np.linspace(*range2), indexing="ij")
    # Use F ordering in flattening to be MATLAB compatible
    x_flat = x.flatten('F')
    y_flat = y.flatten('F')
    return (x, y), (x_flat, y_flat)


def plot_and_pad(u_full, extent, padding):
    u_full_ext = np.pad(u_full, padding)
    plt.imshow(u_full_ext.T, extent=extent, cmap="plasma", aspect=1)
    plt.colorbar()


def plot(u_full, extent, colorbar=True):
    im = plt.imshow(u_full.T, extent=extent, cmap="plasma", aspect=1, interpolation="bilinear")
    if colorbar:
        plt.colorbar()
    return im


def memoized_method(func):
    @functools.wraps(func)
    def wrapped_func(self, *args, **kwargs):
        self_weak = weakref.ref(self)

        @functools.wraps(func)
        @functools.cache
        def cached_method(*args, **kwargs):
            return func(self_weak(), *args, **kwargs)
        setattr(self, func.__name__, cached_method)
        return cached_method(*args, **kwargs)
    return wrapped_func
