
from torch import ShortStorage


class EurlyStopping:
    def __init__(self, patience):
        self.patience = patience
        self.solution = None
        self.previous_error = float("inf")
        self.counter = 0
    
    def check(self, solution, error):
        if error > self.previous_error:
            print("Error increased, waiting...")
            self.counter += 1
        else:
            self.counter = 0
            self.solution = solution
        self.previous_error = error
        if self.counter >= self.patience:
            print("Patience lost")
            return True, self.solution
        else:
            return False, None