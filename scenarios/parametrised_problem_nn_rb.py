from problems.poisson_rhs import Poisson2DGaussianWithParameter
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import keras.callbacks
import scipy.linalg as la

from utils.helper_functions import plot, setup_plots

setup_plots()

# Initialize problem
N = 40
L = 1
split = 0.6

maxiter = 1000
miniter = 50
tolerance = 1e-5

# Get training data
n_samples = 1000
n_basis = 10
basis_type = "fourier"

ps = [-1.0, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]

problems = []
responses_boundary = []
responses = []
basis = pinv = None

xs = []
ys = []

for p in ps:
    problem = Poisson2DGaussianWithParameter(L, L, split, N, N, p=p)
    problems.append(problem)
    basis, system_response, boundary_response, pinv = problem.get_raw_snapshots(n_basis, basis_type)
    responses_boundary.append(boundary_response)
    responses.append(system_response)


# Make reduced basis
U2B, SB, _ = la.svd(np.hstack(responses_boundary), full_matrices=False)
U2, S, _ = la.svd(np.hstack(responses), full_matrices=False)
U2 = U2[:, :n_basis + 1]
U2B = U2B[:, :n_basis + 1]


for problem in problems:
    x, _, yb = problem.get_training_data_reduced_basis(n_samples, n_basis=n_basis, min_val=-10, max_val=10, reduced_basis=(basis, U2, U2B, pinv))
    x = np.vstack((x, problem.p * np.ones((1, x.shape[1]))))
    xs.append(x)
    ys.append(yb)

xs = np.hstack(xs)
ys = np.hstack(ys)

print(f"Input size: {xs.shape[0]}, Output size: {ys.shape[0]}")
np.savez("Poisson2DGaussianParameter_1_1_40_5.npz", x=xs, y=ys)

lambda_reg = 1.422949008978227e-07
n_epochs = 1000
learning_rate = 0.0021517011159379964
activation = "relu"
n_units = 60
length = 5

model = build_FNN(ys.shape[0], activation, n_units, length, partial(l2, lambda_reg))
model.compile(optimizer=Adam(learning_rate=0.002), loss='mean_squared_error', metrics=['mean_squared_error'])

stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=20)
history = model.fit(xs.T, ys.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])

model.summary()

plt.semilogy(history.history['mean_squared_error'], label='train')
plt.semilogy(history.history['val_mean_squared_error'], label='validation')
plt.title("Training results")
plt.ylabel("loss")
plt.xlabel("epoch")
plt.legend()
plt.savefig("training_results.pdf")


def make_plots_for_param(p):
    problem = Poisson2DGaussianWithParameter(L, L, split, N, N, p=p)

    def predictor(x):
        return model.predict(np.hstack((x, problem.p * np.ones((1, x.shape[0])))))

    u_nn, errors_nn = problem.get_nn_solution_boundary(predictor, maxiter=maxiter, tol=tolerance, miniter=miniter, reduced_basis=(basis, U2, U2B, pinv))
    u_rb, errors_rb = problem.get_rb_solution(n_basis, maxiter, tolerance, miniter, basis_type=basis_type)
    u_dd, _ = problem.get_dd_solution(maxiter, tolerance, miniter)

    print(f"NN error: {np.linalg.norm((u_nn-u_dd).flatten())*problem.hx} for p={p}")

    plt.figure()
    plt.semilogy(errors_nn, linestyle="-.", marker=".", label="NN")
    plt.semilogy(errors_rb, linestyle="-.", marker=".", label="RB")
    plt.title("Iteration $L^2$-error")
    plt.xlabel("Number of iterations")
    plt.legend()
    plt.ylabel("$L^2$-error")
    plt.grid()
    plt.savefig(f"iteration_error_{p}.pdf")

    plt.figure()
    plt.subplot(131)
    plt.title("Neural Network based solution")
    plt.imshow(u_nn.T, extent=[0, 0.6, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear")
    plt.colorbar(shrink=0.5)

    plt.subplot(132)
    plt.title("Domain Decomposition solution")
    plt.imshow(u_dd.T, extent=[0, 0.6, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear")
    plt.colorbar(shrink=0.5)

    plt.subplot(133)
    plt.title("Pointwise comparison")
    plt.imshow(np.abs(u_dd - u_nn).T, extent=[0, 0.6, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear")
    plt.colorbar(shrink=0.5)
    plt.tight_layout()
    plt.savefig(f"solution_comparison_{p}.pdf")



test_ps = [-0.8, -0.3, 0.1, 0.65, 1.35, 1.8]

for p in test_ps:
    make_plots_for_param(p)


model.save("trained_neural_network.h5")
