from problems.poisson_rhs import Poisson2DGaussian, Poisson2DPolynomial, Poisson2DSine
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import keras.callbacks
from utils.helper_functions import plot

# Initialize problem
N = 40
L = 1
split = 0.6
problem = Poisson2DGaussian(L, L, split, N, N)


maxiter = 500
miniter = 50
tolerance = 1e-5

n_samples = 1000
basis_type = "fourier"


def assess_input_output(input_size, output_size):
    x, _, yb = problem.get_training_data_reduced_basis(n_samples, input_size, basis_type=basis_type, min_val=-10,
                                                       max_val=10, coefficient_decay=True, decay_type="1/x",
                                                       n_basis_rb=output_size)
    print(f"Input size: {x.shape[0]}, Output size: {yb.shape[0]}")
    model = create_best_network(x, yb)
    nn_error = problem.get_nn_error(model.predict, 'boundary', input_size, maxiter, tolerance, miniter,
                                    basis_type=basis_type, include_zero=False, n_basis_rb=output_size)
    print(f"\tNN Error: {nn_error}")
    return nn_error


def create_best_network(x, yb):
    lambda_reg = 1.422949008978227e-07
    n_epochs = 1000
    learning_rate = 0.0021517011159379964
    activation = "relu"
    n_units = 30
    length = 2

    model = build_FNN(yb.shape[0], activation, n_units, length, partial(l2, lambda_reg))
    model.compile(optimizer=Adam(learning_rate=learning_rate), loss='mean_squared_error', metrics=['mean_squared_error'])

    stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=10)
    model.fit(x.T, yb.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])
    return model


errors = np.zeros((10, 11))
num_avg = 5

for i_in in range(1, 10+1):
    print(f"At {i_in} of 10")
    for j_out in range(1, i_in+2):
        errors[i_in-1, j_out-1] = sum([assess_input_output(i_in, j_out) for _ in range(num_avg)])/num_avg

errors[errors == 0] = np.nan
np.save("errors_gaussian.npy", errors)