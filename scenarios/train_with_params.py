from problems.poisson_rhs import Poisson2DGaussian, Poisson2DPolynomial, Poisson2DSine
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import keras.callbacks


from utils.helper_functions import plot

# Initialize problem
N = 40
L = 1
split = 0.6
problem = Poisson2DSine(L, L, split, N, N)


maxiter = 400
miniter = 50
tolerance = 1e-5

# Get training data
n_samples = 500
n_basis = 5
basis_type = "fourier"

lambda_reg = 0.0011957428541625817
n_epochs = 1000
learning_rate = 0.007536456472926101
activation = "elu"
n_units = 46
length = 3


def train_and_get_error(x, yb, include_zero=False):
    model = build_FNN(yb.shape[0], activation, n_units, length, partial(l2, lambda_reg))
    model.compile(optimizer=Adam(learning_rate=0.0034), loss='mean_squared_error', metrics=['mean_squared_error'])
    stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=10)
    model.fit(x.T, yb.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])
    nn_error = problem.get_nn_error(model.predict, 'boundary', n_basis, maxiter, tolerance, miniter, basis_type=basis_type, include_zero=include_zero)
    return nn_error

# non_zero_errors = []
# for i in range(5):
#     x, y, yb = problem.get_training_data_reduced_basis(n_samples, n_basis, basis_type=basis_type, min_val=-20,
#                                                        max_val=20, coefficient_decay=True, decay_type="1/x", include_zero=False)
#     non_zero_errors.append(train_and_get_error(x, yb))
# print(f"Average nonzero error: {sum(non_zero_errors)/len(non_zero_errors)}")

zero_errors = []
for i in range(5):
    x, y, yb = problem.get_training_data_reduced_basis(n_samples, n_basis, basis_type=basis_type, min_val=-20,
                                                       max_val=20, coefficient_decay=True, decay_type="1/x", include_zero=True)
    zero_errors.append(train_and_get_error(x, yb, include_zero=True))
print(f"Average zero error: {sum(zero_errors)/len(zero_errors)}")