from problems.poisson_rhs import Poisson2DGaussian, Poisson2DPolynomial, Poisson2DSine, make_custom_poisson_factory_static, calculate_gaussian_sol
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import keras.callbacks


from utils.helper_functions import plot, setup_plots

setup_plots()

# Initialize problem
N = 40
L = 1
split = 0.6
problem = Poisson2DPolynomial(L, L, split, N, N)


maxiter = 1000
miniter = 50
tolerance = 1e-5

# Get training data
n_samples = 2000
n_basis = 10
basis_type = "fourier"
x, y, yb = problem.get_training_data_reduced_basis(n_samples, n_basis, basis_type=basis_type, min_val=-10, max_val=10, coefficient_decay=False, decay_type="1/x", include_zero=False)
print(f"Input size: {x.shape[0]}, Output size: {yb.shape[0]}")
#np.savez("Poisson2DGaussian_1_1_40_5.npz", x=x, y=y, yb=yb)
#exit()
lambda_reg = 1.422949008978227e-07
n_epochs = 1000
learning_rate = 0.0021517011159379964
activation = "relu"
n_units = 30
length = 2

# Whole network


def do_whole():
    model = build_FNN(y.shape[0], activation, n_units, length, partial(l2, lambda_reg))
    model.compile(optimizer=Adam(learning_rate=learning_rate), loss='mean_squared_error', metrics=['mean_squared_error'])

    stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=10)
    model.fit(x.T, y.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])
    nn_error = problem.get_nn_error(model.predict, 'whole', n_basis, maxiter, tolerance, miniter, basis_type=basis_type, include_zero=False)
    return nn_error


def do_boundary():
    model = build_FNN(yb.shape[0], activation, n_units, length, partial(l2, lambda_reg))
    model.compile(optimizer=Adam(learning_rate=learning_rate), loss='mean_squared_error',
                  metrics=['mean_squared_error'])

    stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=10)
    model.fit(x.T, yb.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])
    nn_error = problem.get_nn_error(model.predict, 'boundary', n_basis, maxiter, tolerance, miniter, basis_type=basis_type,
                                    include_zero=False)
    return nn_error


num_avg = 5
results_whole = sum([do_whole() for _ in range(num_avg)])/num_avg
print(f"Average whole error: {results_whole}")

results_boundary = sum([do_boundary() for _ in range(num_avg)])/num_avg
print(f"Average boundary error: {results_boundary}")