from keras.models import load_model
import matplotlib.pyplot as plt
from utils.plot_settings import setup_plots
import numpy as np

setup_plots()

model = load_model("./test_runs/Gaussian/trained_neural_network.h5")

weights = model.layers[1].weights[0]
biases = model.layers[1].weights[1]

fig, ax = plt.subplots(1, 2, gridspec_kw={'width_ratios': [3, 1]})

plt.subplot(1, 2, 1)
plt.imshow(weights)
plt.xlabel("Output")
plt.ylabel("Input")
plt.xticks(range(0, 11), range(1, 12))
plt.yticks(range(0, 10), range(1, 11))
plt.colorbar(shrink=0.9)

plt.subplot(1, 2, 2)
plt.imshow(np.array(biases).reshape((-1, 1)))
plt.ylabel("Output")
plt.yticks(range(0, 11), range(1, 12))
plt.xticks([])
plt.colorbar(shrink=0.9)

plt.suptitle("Visualisation of weights and biases")
plt.tight_layout()

plt.savefig("visualisation_of_paramaters_gaussian.pdf")

