import numpy as np
import matplotlib.pyplot as plt

errors = np.load("errors_gaussian.npy")[1:, 1:]
y = list(range(2, 10+1))
x = list(range(2, 12))
plt.imshow(errors)
plt.xticks(range(0, 10), x)
plt.yticks(range(0, 9), y)
plt.xlabel("Output size")
plt.ylabel("Input size")
plt.gca().invert_yaxis()
plt.colorbar()

for i in range(9):
    for j in range(10):
        text = plt.text(j, i, f"{errors[i, j]:1.1e}",
                       ha="center", va="center", color="w")
plt.show()