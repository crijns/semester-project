from problems.poisson_rhs import Poisson2DGaussian, Poisson2DPolynomial, Poisson2DSine, make_custom_poisson_factory_static, calculate_gaussian_sol
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
from keras.models import load_model
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import keras.callbacks


from utils.helper_functions import plot, setup_plots

setup_plots()

# Initialize problem
N = 40
L = 1
split = 0.6
problem = Poisson2DGaussian(L, L, split, N, N)


maxiter = 1000
miniter = 50
tolerance = 1e-5

# Get training data
n_samples = 2000
n_basis = 10
basis_type = "fourier"
x, y, yb = problem.get_training_data_reduced_basis(n_samples, n_basis, basis_type=basis_type, min_val=-10, max_val=10, coefficient_decay=True, decay_type="1/x", include_zero=False)
print(f"Input size: {x.shape[0]}, Output size: {yb.shape[0]}")
#np.savez("Poisson2DGaussian_1_1_40_5.npz", x=x, y=y, yb=yb)
#exit()
lambda_reg = 1.422949008978227e-07
n_epochs = 1000
learning_rate = 0.0021517011159379964
activation = "relu"
n_units = 30
length = 2

model = build_FNN(yb.shape[0], activation, n_units, length, partial(l2, lambda_reg))
model.compile(optimizer=Adam(learning_rate=learning_rate), loss='mean_squared_error', metrics=['mean_squared_error'])

stop_early = keras.callbacks.EarlyStopping(monitor="val_mean_squared_error", patience=10)
history = model.fit(x.T, yb.T, epochs=n_epochs, validation_split=0.2, callbacks=[stop_early])

model.summary()
# model = load_model("trained_neural_network.h5")

# plt.semilogy(history.history['mean_squared_error'], label='train')
# plt.semilogy(history.history['val_mean_squared_error'], label='validation')
# plt.title("Training results")
# plt.ylabel("loss")
# plt.xlabel("epoch")
# plt.legend()
# plt.savefig("training_results.pdf")

u_nn, errors_nn = problem.get_nn_solution_whole(model.predict, n_basis, maxiter, tolerance, miniter, basis_type=basis_type, include_zero=False)
print(errors_nn)
u_rb, errors_rb = problem.get_rb_solution(n_basis, maxiter, tolerance, miniter, basis_type=basis_type)
u_dd, _ = problem.get_dd_solution(maxiter, tolerance, miniter)

plt.figure()
plt.semilogy(errors_nn, linestyle="-.", marker=".", label="NN")
plt.semilogy(errors_rb, linestyle="-.", marker=".", label="RB")
plt.title("Iteration $L^2$-error")
plt.xlabel("Number of iterations")
plt.legend()
plt.ylabel("$L^2$-error")
plt.grid()
plt.savefig("iteration_error.pdf")

plt.figure()
plt.subplot(121)
plt.title("Neural Network based solution")
plot(u_nn, [0, 0.6, 0, 1])

plt.subplot(122)
plt.title("Domain Decomposition solution")
plot(u_dd, [0, 0.6, 0, 1])
plt.tight_layout()
plt.savefig("solution_comparison.pdf")

plt.figure()
plt.title("Pointwise comparison")
plot(np.abs(u_dd-u_nn), [0, 0.6, 0, 1])
plt.savefig("error_solution_comparison.pdf")


correction_factor = np.sqrt(problem.hx * problem.hy)
nn_error = problem.get_nn_error(model.predict, 'whole', n_basis, maxiter, tolerance, miniter, basis_type=basis_type, include_zero=False)

reduced_error = problem.get_reduced_error(n_basis, maxiter, tolerance, miniter, basis_type=basis_type)

print(f"NN Error: {nn_error}, Reduced Error: {reduced_error}")


#model.save("trained_neural_network.h5")
