import keras.callbacks
from neural_nets.networks import build_FNN
from tensorflow.keras.regularizers import l2
from tensorflow.keras.optimizers import RMSprop, Adam
import keras_tuner as kt
from functools import partial
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from utils.helper_functions import plot


# Load data
data = np.load("Poisson2DGaussian_1_1_40_5.npz")
x = data['x']
y = data['y']
yb = data['yb']

output_shape = y.shape[0]


def build_model(hp):
    lambda_reg = hp.Float('lambda', min_value=1e-7, max_value=1e-2, sampling="log")
    learning_rate = hp.Float("learning_rate", min_value=1e-4, max_value=1e-2, sampling="log")
    regularizer = partial(l2, lambda_reg)
    model = build_FNN(output_shape,
                      hp.Choice('activation', ['relu', 'elu', 'sigmoid']),
                      hp.Int('n_units', min_value=1, max_value=50, step=1),
                      hp.Int('length', min_value=2, max_value=6),
                      regularizer)
    model.compile(optimizer=Adam(learning_rate=learning_rate), loss='mean_squared_error',
                  metrics=['mean_squared_error'])
    return model


x_train = x
y_train = yb

stop_early = keras.callbacks.EarlyStopping(monitor="val_loss", patience=10)

tuner = kt.Hyperband(build_model,
                     objective='val_loss',
                     directory="./search",
                     project_name="Poisson2DGaussian_40_5",
                     overwrite=True,
                     executions_per_trial=3)
tuner.search_space_summary()
tuner.search(x_train.T, y_train.T, validation_split=0.2, callbacks=[stop_early, keras.callbacks.TensorBoard("./tb_logs")])
tuner.results_summary()
