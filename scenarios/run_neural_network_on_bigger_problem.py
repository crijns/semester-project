from problems.poisson_rhs import Poisson2DSine
from tensorflow.keras.models import load_model
import matplotlib.pyplot as plt
import numpy as np
from utils.helper_functions import plot

# Initialize problem
N = 400
L = 1
split = 0.6
problem = Poisson2DSine(L, L, split, N, N)

maxiter = 50
miniter = 5
tolerance = 1e-5

# Get training data
n_basis = 5
basis_type = "fourier"

# Load model

model = load_model("PoissonSineFourier_n=5.h5")

u_nn, errors_nn = problem.get_nn_solution_whole(model.predict, n_basis, maxiter, tolerance, miniter, basis_type=basis_type)
u_rb, errors_rb = problem.get_rb_solution(n_basis, maxiter, tolerance, miniter, basis_type=basis_type)
u_dd, _ = problem.get_dd_solution(maxiter, tolerance, miniter)

plt.figure()
plt.semilogy(errors_nn, linestyle="-.", marker=".", label="NN")
plt.semilogy(errors_rb, linestyle="-.", marker=".", label="RB")
plt.title("Cauchy $L^2$-error")
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.grid()

plt.figure(figsize=(9, 4))
plt.subplot(121)
plt.title("Neural Network based solution")
plot(u_nn, [0, 0.6, 0, 1])

plt.subplot(122)
plt.title("Domain Decomposition solution")
plot(u_dd, [0, 0.6, 0, 1])
plt.tight_layout()
plt.show()


correction_factor = np.sqrt(problem.hx * problem.hy)
nn_error = np.linalg.norm(u_nn.flatten()-u_dd.flatten())*correction_factor
reduced_error = np.linalg.norm(u_rb.flatten()-u_dd.flatten())*correction_factor

print(f"NN Error: {nn_error}, Reduced Error: {reduced_error}")
