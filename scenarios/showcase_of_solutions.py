from problems.poisson_rhs import *
from utils.helper_functions import plot
import matplotlib.pyplot as plt
from utils.plot_settings import setup_plots

setup_plots(1.2, square=True, width_scale=1)


def do_problem(problem, name):
    u_exact_sol = problem.get_exact_solution()
    plt.figure()
    plot(u_exact_sol, [0, 1, 0, 1])
    plt.axvline(0.6)
    plt.xlabel("$x$")
    plt.ylabel("$y$")
    #plt.tight_layout()
    plt.title("Exact Solution")
    plt.savefig(name)


problem = Poisson2DPolynomial(1, 1, 0.6, 40, 40)
do_problem(problem, "polynomial_example.pdf")

problem = Poisson2DSine(1, 1, 0.6, 40, 40)
do_problem(problem, "sine_example.pdf")

problem = Poisson2DGaussian(1, 1, 0.6, 40, 40)
do_problem(problem, "gaussian_example.pdf")
