import numpy as np

from problems.poisson_rhs import *
from problems.poisson import Poisson2DAbstract
from utils.helper_functions import plot
import matplotlib.pyplot as plt
from utils.plot_settings import setup_plots

setup_plots(1, square=False, width_scale=1)


def do_problem(problem: Poisson2DAbstract, name):
    basis_f, U2_f, S_f, U2B_f, SB_f, pinv_f = problem.get_reduced_basis(n_basis=5, basis_type="fourier")
    basis_l, U2_l, S_l, U2B_l, SB_l, pinv_l = problem.get_reduced_basis(n_basis=5, basis_type="legendre")
    fig = plt.figure()
    for i in range(4):
        ax = fig.add_subplot(1, 4, i+1)
        ax.set_title(f"$u_\u007b{i}\u007d$ for $\sigma_\u007b{i}\u007d={S_f[i]:1.2f}$")
        u = np.pad(U2_f[:, i].reshape((problem.nx2, problem.ny-1)), ((0, 1), (1, 1)))
        im = ax.imshow(u.T, extent=[0.6, 1, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear", clim=(np.min(U2_f[:, :4].flatten()), np.max(U2_f[:, :4].flatten())))
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        ax.set_xticks([0.6, 1.0])
    fig.suptitle("First 4 singular vectors for $P_2$")
    plt.tight_layout()
    fig.colorbar(im, ax=fig.get_axes(), shrink=0.8)
    plt.savefig(name+"_fourier.pdf")
    fig = plt.figure()
    for i in range(4):
        ax = fig.add_subplot(1, 4, i+1)
        ax.set_title(f"$u_\u007b{i}\u007d$ for $\sigma_\u007b{i}\u007d={S_l[i]:1.2f}$")
        u = np.pad(U2_l[:, i].reshape((problem.nx2, problem.ny-1)), ((0, 1), (1, 1)))
        im = ax.imshow(u.T, extent=[0.6, 1, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear", clim=(np.min(U2_l[:, :4].flatten()), np.max(U2_l[:, :4].flatten())))
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        ax.set_xticks([0.6, 1.0])
    fig.suptitle("First 4 singular vectors for $P_2$")
    plt.tight_layout()
    fig.colorbar(im, ax=fig.get_axes(), shrink=0.8)
    plt.savefig(name+"_legendre.pdf")
    fig = plt.figure()
    for i in range(4):
        ax = fig.add_subplot(1, 4, i+1)
        u1 = np.abs(U2_l[:, i] - U2_f[:, i])
        u2 = np.abs(U2_f[:, i] - U2_l[:, i])
        u = u1 if np.linalg.norm(u1.flatten()) < np.linalg.norm(u2.flatten()) else u2
        u = np.pad(u.reshape((problem.nx2, problem.ny-1)), ((0, 1), (1, 1)))
        im = ax.imshow(u.T, extent=[0.6, 1, 0, 1], cmap="plasma", aspect=1, interpolation="bilinear", clim=(0.0, 0.2))
        ax.set_xlabel("$x$")
        ax.set_ylabel("$y$")
        ax.set_xticks([0.6, 1.0])
    fig.suptitle("First 4 singular vector difference for $P_2$")
    plt.tight_layout()
    fig.colorbar(im, ax=fig.get_axes(), shrink=0.8)
    plt.savefig(name+"_difference.pdf")


problem = Poisson2DPolynomial(1, 1, 0.6, 40, 40)
do_problem(problem, "polynomial_rb_modes")

problem = Poisson2DSine(1, 1, 0.6, 40, 40)
do_problem(problem, "sine_rb_modes")

problem = Poisson2DGaussian(1, 1, 0.6, 40, 40)
do_problem(problem, "gaussian_rb_modes")
