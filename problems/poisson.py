"""
Poisson's equation
"""
from abc import ABC, abstractmethod

import numpy as np
import scipy.linalg as la
import scipy.sparse as sp
import scipy.sparse.linalg as ls
from scipy.special import legendre
from scipy.stats.qmc import LatinHypercube
from tqdm import tqdm

from utils.helper_functions import fd_laplacian, flattened_ij_meshgrid
from utils.helper_functions import memoized_method
from utils.plain_earlystopping import EurlyStopping

inverse_epsilon = 1e10


class Poisson2DAbstract(ABC):
    def __init__(self, lx, ly, split_percentage, nx, ny):
        # Variable initialization
        self.lx = lx
        self.ly = ly
        self.split_percentage = split_percentage
        self.nx = nx
        self.ny = ny
        self.hx = lx / nx
        self.hy = ly / ny

        # Discretisation
        (_, _), (self.x_flat, self.y_flat) = flattened_ij_meshgrid((0, lx, nx + 1),
                                                                   (0, ly, ny + 1))
        (_, _), (self.x_int_flat, self.y_int_flat) = flattened_ij_meshgrid((self.hx, lx - self.hx, nx - 1),
                                                                           (self.hy, ly - self.hy, ny - 1))
        self.nx1 = round(split_percentage * nx)
        self.nx2 = nx - self.nx1

        # Separation of subdomains
        (_, _), (self.x1_flat, self.y1_flat) = flattened_ij_meshgrid((0, split_percentage * lx, self.nx1 + 1),
                                                                     (0, ly, ny + 2))
        (_, _), (self.x2_flat, self.y2_flat) = flattened_ij_meshgrid(
            (split_percentage * lx, (1 - split_percentage) * lx, self.nx2 + 1),
            (0, ly, ny + 2))

        # Problem Matrices
        self.Ax = fd_laplacian(self.nx - 1, self.hx)
        self.Ay = fd_laplacian(self.ny - 1, self.hy)
        self.A = sp.kron(self.Ax, sp.eye(self.ny - 1)) + sp.kron(sp.eye(self.nx - 1), self.Ay)

        # Subproblem Matrices
        # Problem 1
        self.A1x = fd_laplacian(self.nx1, self.hx)
        self.A1y = fd_laplacian(self.ny - 1, self.hy)
        self.A1 = sp.kron(self.A1x, sp.eye(self.ny - 1)) + sp.kron(sp.eye(self.nx1), self.A1y)
        # Correct Dirichlet BC
        self.A1[-self.ny + 1:, -self.ny + 1:] = self.A1[-self.ny + 1:,
                                                -self.ny + 1:] / 2 + inverse_epsilon * sp.eye(self.ny - 1)

        # Problem 2
        self.A2x = fd_laplacian(self.nx2, self.hx)
        self.A2y = fd_laplacian(self.ny - 1, self.hy)
        self.A2 = sp.kron(self.A2x, sp.eye(self.ny - 1)) + sp.kron(sp.eye(self.nx2), self.A2y)
        # Correct for Neumann
        self.A2[:self.ny - 1, :self.ny - 1] /= 2

        # Coupling Matrices
        self.C1x = sp.dok_matrix((self.nx1, self.nx2))
        self.C1x[-1, 0] = inverse_epsilon
        # Force CSC sparse format, as CSR format does not support slicing
        self.C1 = sp.kron(self.C1x, sp.eye(self.ny - 1), format="csc")
        self.C1 = -self.C1

        self.C2x = sp.dok_matrix((self.nx2, self.nx1))
        self.C2x[0, -1] = -3 / (2 * self.hx ** 2)
        self.C2x[0, -2] = 2 / (self.hx ** 2)
        self.C2x[0, -3] = -1 / (2 * self.hx ** 2)
        # Force CSC sparse format, as CSR format does not support slicing
        self.C2 = sp.kron(self.C2x, sp.eye(self.ny - 1), format="csc")
        self.C2 = -self.C2

        # Subproblems RHS
        self.rhs_sub = \
            np.hstack(
                (self.f(self.x_int_flat[:(self.ny - 1) * self.nx1], self.y_int_flat[:(self.ny - 1) * self.nx1]),
                 self.f(self.x_int_flat[(self.ny - 1) * (self.nx1 - 1):],
                        self.y_int_flat[(self.ny - 1) * (self.nx1 - 1):])
                 ))[..., np.newaxis]
        # Correct for Dirichlet
        self.rhs_sub[(self.ny - 1) * (self.nx1 - 1):(self.ny - 1) * self.nx1] /= 2
        # Correct for Neumann
        self.rhs_sub[(self.ny - 1) * self.nx1:(self.ny - 1) * (self.nx1 + 1)] /= 2

    def __eq__(self, other):
        return self.lx == other.lx and self.ly == other.ly and self.split_percentage == other.split_percentage and self.nx == other.nx and self.ny == other.ny and self.n_exact == other.n_exact and self.m_exact == other.m_exact

    def __hash__(self):
        return hash((self.lx, self.ly, self.split_percentage, self.nx, self.ny, self.n_exact, self.m_exact))

    def _reshape_and_pad(self, sol):
        return np.pad(sol.reshape((self.nx - 1, self.ny - 1)), ((1, 1), (1, 1)))

    def _join_and_pad12(self, u1, u2):
        return np.pad(np.vstack((u1.reshape(self.nx1, self.ny - 1), u2.reshape(self.nx2, self.ny - 1)[1:, :])),
                      ((1, 1), (1, 1)))

    @memoized_method
    def get_fd_solution(self):
        rhs = self.f(self.x_int_flat, self.y_int_flat)
        u = ls.spsolve(self.A, rhs)
        return self._reshape_and_pad(u)

    @memoized_method
    def get_dd_solution(self, maxiter=400, tol=None, miniter=None):
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            u2_guess = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[0]:] +
                                  np.hstack(
                                      (1 / self.hx * neumann_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                      ..., np.newaxis])
            dirichlet_values = u2_guess[:self.ny - 1]
            errors[i] = np.linalg.norm(u1_guess_new - u1_guess) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break
        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    @memoized_method
    def get_poincare_steklov_operator(self):
        S = np.zeros((self.ny - 1, self.ny - 1))
        for j in range(self.ny - 1):
            e_basis = np.zeros((self.ny - 1, 1))
            e_basis[j] = 1
            e_basis_extended = np.vstack((e_basis, np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))
            solution = ls.spsolve(self.A2, 0 + 1 / self.hx * e_basis_extended)
            S[:, j] = solution[:self.ny - 1]
        # Affine part
        solution = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[0]:])
        S0 = solution[:self.ny - 1]
        return S, S0

    @memoized_method
    def get_operator_solution(self, maxiter=400, tol=None, miniter=None):
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)
        S, S0 = self.get_poincare_steklov_operator()

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            dirichlet_values = S0 + S @ neumann_values
            errors[i] = np.linalg.norm(u1_guess_new - u1_guess) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    @memoized_method
    def _make_fourier_basis(self, n_basis):
        # Include the 0 function in the basis
        # So size is (N x (n_basis+1))
        return np.sin(
            (np.arange(0, n_basis + 1).reshape(1, -1) * np.pi / self.ly * np.linspace(self.hy, self.ly - self.hy,
                                                                                      self.ny - 1).reshape(-1, 1)))

    @memoized_method
    def _make_legendre_basis(self, n_basis):
        # Include the 0 function in the basis
        # So size is (N x (n_basis+1))
        evaluated_at = np.linspace(self.hy, self.ly - self.hy, self.ny - 1)
        funcs = [lambda x: np.zeros(x.shape)] + [legendre(i) for i in range(0, n_basis - 1)]
        basis = np.zeros((len(evaluated_at), n_basis + 1))
        for i, func in enumerate(funcs):
            basis[:, i] = func(evaluated_at / self.ly)
        return basis

    @memoized_method
    def get_reduced_basis(self, n_basis=5, basis_type="fourier", include_zero=False, n_basis_rb=None):
        if basis_type == "fourier":
            basis = self._make_fourier_basis(n_basis)
        elif basis_type == "legendre":
            basis = self._make_legendre_basis(n_basis)
        else:
            raise ValueError("Incorrect Basis type")
        system_response_solution = np.zeros((self.A2.shape[0], n_basis + 1))
        system_response_boundary = np.zeros((self.ny - 1, n_basis + 1))
        for j in range(n_basis + 1):
            boundary_vector = basis[:, j]
            boundary_vector_extended = np.vstack(
                (boundary_vector[..., np.newaxis], np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))
            system_response_solution[:, j] = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[
                                                                                  0]:] + 1 / self.hx * boundary_vector_extended)
            system_response_boundary[:, j] = system_response_solution[:self.ny - 1, j]
        # SVDs of system response and boundary
        U2, S, _ = la.svd(system_response_solution, full_matrices=False)
        # Only take n_basis latent vectors for reduced basis
        # U2 = U2[:, :n_basis]
        U2B, SB, _ = la.svd(system_response_boundary, full_matrices=False)
        # Only take n_basis latent vectors for reduced basis
        # U2B = U2B[:, :n_basis]
        # sp.eye(U2.shape[0], format="csc")[:, :n_basis]
        # Change reduced_basis output if specified
        if n_basis_rb:
            U2 = U2[:, :n_basis_rb]
            U2B = U2B[:, :n_basis_rb]
        else:
            U2 = U2[:, :n_basis + 1]
            U2B = U2B[:, :n_basis + 1]
        if include_zero:
            return basis, U2, S, U2B, SB, np.vstack((np.zeros((1, basis[:, 1:].shape[0])), la.pinv(basis[:, 1:])))
        else:
            return basis[:, 1:], U2, S, U2B, SB, la.pinv(basis[:, 1:])

    def get_training_data_reduced_basis(self, n_samples, n_basis=5, min_val=-100, max_val=100, basis_type="fourier",
                                        coefficient_decay=False, decay_type="1/x", include_zero=False, n_basis_rb=None,
                                        reduced_basis=None):
        if not reduced_basis:
            basis, U2, _, U2B, _, _ = self.get_reduced_basis(n_basis, basis_type, include_zero, n_basis_rb)
        else:
            basis, U2, U2B, _ = reduced_basis
        if include_zero:
            n_basis += 1
        sampler = LatinHypercube(n_basis)
        coefficients = min_val + (sampler.random(n_samples) * (max_val - min_val)).T
        if coefficient_decay:
            if decay_type == "1/x":
                decay_factors = (1 / np.arange(1, n_basis + 1))[..., np.newaxis]
            elif decay_type == "exp(-x+1)":
                decay_factors = np.exp(-np.arange(1, n_basis + 1) + 1)[..., np.newaxis]
            else:
                raise ValueError("Improper decay type")
            coefficients = coefficients * decay_factors

        boundaries = basis @ coefficients

        output_system_response = np.zeros((self.A2.shape[0], n_samples))
        output_system_response_boundary = np.zeros((self.ny - 1, n_samples))

        for j in tqdm(range(n_samples)):
            boundary_vector = boundaries[:, j]
            boundary_vector_extended = np.vstack(
                (boundary_vector[..., np.newaxis], np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))
            output_system_response[:, j] = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[
                                                                                0]:] + 1 / self.hx * boundary_vector_extended)
            output_system_response_boundary[:, j] = output_system_response[:self.ny - 1, j]

        return coefficients, U2.T @ output_system_response, U2B.T @ output_system_response_boundary

    @memoized_method
    def get_raw_snapshots(self, n_basis=5, basis_type="fourier"):
        if basis_type == "fourier":
            basis = self._make_fourier_basis(n_basis)
        elif basis_type == "legendre":
            basis = self._make_legendre_basis(n_basis)
        else:
            raise ValueError("Incorrect Basis type")
        system_response_solution = np.zeros((self.A2.shape[0], n_basis + 1))
        system_response_boundary = np.zeros((self.ny - 1, n_basis + 1))
        for j in range(n_basis + 1):
            boundary_vector = basis[:, j]
            boundary_vector_extended = np.vstack(
                (boundary_vector[..., np.newaxis], np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))
            system_response_solution[:, j] = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[
                                                                                  0]:] + 1 / self.hx * boundary_vector_extended)
            system_response_boundary[:, j] = system_response_solution[:self.ny - 1, j]

        return basis[:, 1:], system_response_solution, system_response_boundary, la.pinv(basis[:, 1:])


    def get_original_basis(self, basis_type, n_basis_in, n_basis_out):
        if basis_type == "fourier":
            basis = self._make_fourier_basis(max(n_basis_in, n_basis_out))[:, 1:]
        elif basis_type == "legendre":
            basis = self._make_legendre_basis(max(n_basis_in, n_basis_out))[:, 1:]
        else:
            raise ValueError("Wrong basis type")
        return basis[:, :n_basis_in], basis[:, :n_basis_out], la.pinv(basis[:, :n_basis_in]), la.pinv(
            basis[:, :n_basis_out])

    def get_training_data_original_basis(self, n_samples, min_val=-100, max_val=100, basis_type="fourier",
                                         coefficient_decay=False, decay_type="1/x", n_basis_in=5, n_basis_out=5):
        basis_in, _, _, basis_out_pinv = self.get_original_basis(basis_type, n_basis_in, n_basis_out)
        sampler = LatinHypercube(n_basis_in)
        coefficients = min_val + (sampler.random(n_samples) * (max_val - min_val)).T
        if coefficient_decay:
            if decay_type == "1/x":
                decay_factors = (1 / np.arange(1, n_basis_in + 1))[..., np.newaxis]
            elif decay_type == "exp(-x+1)":
                decay_factors = np.exp(-np.arange(1, n_basis_in + 1) + 1)[..., np.newaxis]
            else:
                raise ValueError("Improper decay type")
            coefficients = coefficients * decay_factors

        boundaries = basis_in @ coefficients
        output_system_response = np.zeros((self.A2.shape[0], n_samples))
        output_system_response_boundary = np.zeros((n_basis_out, n_samples))

        for j in tqdm(range(n_samples)):
            boundary_vector = boundaries[:, j]
            boundary_vector_extended = np.vstack(
                (boundary_vector[..., np.newaxis], np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))
            output_system_response[:, j] = ls.spsolve(self.A2, self.rhs_sub[self.A1.shape[
                                                                                0]:] + 1 / self.hx * boundary_vector_extended)
            output_system_response_boundary[:, j] = basis_out_pinv @ output_system_response[:self.ny - 1, j]
        return coefficients, output_system_response_boundary

    @memoized_method
    def get_rb_solution(self, n_basis=5, maxiter=400, tol=None, miniter=None, basis_type="fourier"):
        _, U2, _, U2B, _, _ = self.get_reduced_basis(n_basis, basis_type)
        A2R = U2.T @ self.A2 @ U2
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            dirichlet_values = U2[:self.ny - 1, :] @ ls.spsolve(A2R,
                                                                U2.T @ (self.rhs_sub[self.A1.shape[0]:] + np.vstack(
                                                                    (1 / self.hx * neumann_values[..., np.newaxis],
                                                                     np.zeros(((self.nx2 - 1) * (self.ny - 1), 1))))))
            errors[i] = np.linalg.norm(u1_guess_new - u1_guess) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    @memoized_method
    def get_rb_solution_boundary(self, n_basis=5, maxiter=400, tol=None, miniter=None, basis_type="fourier"):
        basis, _, _, U2B, _, pinv_basis = self.get_reduced_basis(n_basis, basis_type)
        S, S0 = self.get_poincare_steklov_operator()
        Sred = S @ basis
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            # Should this be U2B.T or pinv?
            dirichlet_values = S0 + Sred @ (pinv_basis @ neumann_values)
            errors[i] = np.linalg.norm(u1_guess_new - u1_guess) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    @abstractmethod
    def f(self, x, y):
        raise NotImplementedError("This method should be overridden")

    def get_exact_solution(self):
        return self._reshape_and_pad(self.exact_solution_coordinate_wise(self.x_int_flat, self.y_int_flat))

    @abstractmethod
    def exact_solution_coordinate_wise(self, x, y):
        raise NotImplementedError("This method should be overridden")

    def get_nn_solution_whole(self, predictor, n_basis=5, maxiter=400, tol=None, miniter=None, basis_type="fourier",
                              patience=10, include_zero=False, n_basis_rb=None, reduced_basis=None):
        if not reduced_basis:
            _, U2, _, _, _, pinv = self.get_reduced_basis(n_basis, basis_type, include_zero, n_basis_rb)
        else:
            _, U2, _, pinv = reduced_basis
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        criterion = EurlyStopping(patience)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            # if i % 50 == 0:
            #     tmp_sol = np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1)))
            #     plt.title("NN sol")
            #     plot(tmp_sol, [0, 0.6, 0, 1])
            #     plt.tight_layout()
            #     plt.figure()
            #     plot(np.pad((u1_guess_new - u1_guess).reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), [0, 0.6, 0, 1])
            #     plt.show()
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            dirichlet_values = U2[:self.ny - 1, :] @ predictor((pinv @ neumann_values)[..., np.newaxis].T).reshape(-1)
            errors[i] = np.linalg.norm(u1_guess_new.flatten() - u1_guess.flatten()) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break
            res, sol = criterion.check(u1_guess, errors[i])
            if res:
                u1_guess = sol
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    def get_nn_solution_boundary(self, predictor, n_basis=5, maxiter=400, tol=None, miniter=None, basis_type="fourier",
                                 patience=10, include_zero=False, n_basis_rb=None, reduced_basis=None):
        if not reduced_basis:
            _, _, _, U2B, _, pinv = self.get_reduced_basis(n_basis, basis_type, include_zero, n_basis_rb)
        else:
            _, _, U2B, pinv = reduced_basis
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        criterion = EurlyStopping(patience)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            # if i % 50 == 0:
            #     tmp_sol = np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1)))
            #     plt.title("Domain Decomposition solution")
            #     plot(tmp_sol, [0, 0.6, 0, 1])
            #     plt.tight_layout()
            #     plt.show()
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            dirichlet_values = U2B @ predictor((pinv @ neumann_values)[..., np.newaxis].T).reshape(-1)
            errors[i] = np.linalg.norm(u1_guess_new.flatten() - u1_guess.flatten()) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break
            res, sol = criterion.check(u1_guess, errors[i])
            if res:
                u1_guess = sol
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    def get_nn_solution_original_basis(self, predictor, maxiter=400, tol=None, miniter=None,
                                       basis_type="fourier", n_basis_in=5, n_basis_out=5, patience=10):

        basis_in, basis_out, pinv_basis_in, pinv_basis_out = self.get_original_basis(basis_type, n_basis_in,
                                                                                     n_basis_out)
        dirichlet_values = np.zeros(self.ny - 1)
        u1_guess = np.zeros((self.ny - 1) * self.nx1)
        errors = np.zeros(maxiter)

        criterion = EurlyStopping(patience)

        for i in tqdm(range(maxiter)):
            u1_guess_new = ls.spsolve(self.A1, self.rhs_sub[:self.A1.shape[0]] - self.C1 @
                                      np.hstack((dirichlet_values, np.zeros((self.nx2 - 1) * (self.ny - 1))))[
                                          ..., np.newaxis])
            # if i % 50 == 0:
            #     tmp_sol = np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1)))
            #     plt.title("Domain Decomposition solution")
            #     plot(tmp_sol, [0, 0.6, 0, 1])
            #     plt.tight_layout()
            #     plt.show()
            neumann_values = -self.hx * self.C2[:self.ny - 1, :] @ u1_guess_new
            dirichlet_values = basis_out @ predictor((pinv_basis_in @ neumann_values)[..., np.newaxis].T).reshape(-1)
            errors[i] = np.linalg.norm(u1_guess_new.flatten() - u1_guess.flatten()) * np.sqrt(self.hx * self.hy)
            u1_guess = u1_guess_new
            if tol and errors[i] < tol and (miniter and i > miniter):
                errors = errors[:i + 1]
                break
            res, sol = criterion.check(u1_guess, errors[i])
            if res:
                u1_guess = sol
                break

        return np.pad(u1_guess.reshape((self.nx1, self.ny - 1)), ((1, 0), (1, 1))), errors

    def get_reduced_error(self, n_basis=5, maxiter=400, tol=None, miniter=None, basis_type="fourier"):
        u_rb, _ = self.get_rb_solution(n_basis, maxiter, tol, miniter, basis_type)
        u_dd, _ = self.get_dd_solution(maxiter, tol, miniter)
        correction_factor = np.sqrt(self.hx * self.hy)
        return np.linalg.norm(u_rb.flatten() - u_dd.flatten()) * correction_factor

    def get_nn_error(self, predictor, predictor_type="whole", n_basis=5, maxiter=400, tol=None, miniter=None,
                     basis_type="fourier", patience=10, include_zero=False, n_basis_rb=None, reduced_basis=None):
        if predictor_type == "whole":
            u_nn, _ = self.get_nn_solution_whole(predictor, n_basis, maxiter, tol, miniter, basis_type, patience,
                                                 include_zero, n_basis_rb, reduced_basis)
        elif predictor_type == "boundary":
            u_nn, _ = self.get_nn_solution_boundary(predictor, n_basis, maxiter, tol, miniter, basis_type, patience,
                                                    include_zero, n_basis_rb, reduced_basis)
        else:
            raise ValueError("Wrong predictor_type, choose one of: 'whole' or 'boundary'")
        u_dd, _ = self.get_dd_solution(maxiter, tol, miniter)
        correction_factor = np.sqrt(self.hx * self.hy)
        return np.linalg.norm(u_nn.flatten() - u_dd.flatten()) * correction_factor
