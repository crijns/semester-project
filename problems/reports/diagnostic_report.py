import numpy as np
import toml
import pprint
import os
import shutil
from datetime import datetime
from pydoc import locate
from utils.plot_settings import setup_plots
import matplotlib.pyplot as plt

setup_plots(1.0)

config_path = "diagnostics_config.toml"

config = toml.load(config_path)

maxiter = config["criteria"]["maxiter"]
miniter = config["criteria"]["miniter"]
tolerance = config["criteria"]["tolerance"]

dirname = "Report at " + str(datetime.now()) + "/"
topdir = os.path.join(os.getcwd(), dirname)
os.mkdir(topdir)
shutil.copy(config_path, os.path.join(topdir, config_path))

if "convergence" in config and config["convergence"]["run"]:
    conv_conf = config["convergence"]
    print("Executing convergence diagnostic report...")
    print("Using config: ")
    pprint.pprint(conv_conf)

    Ns = np.array(conv_conf["N"])
    basis_type = conv_conf["basis_type"]
    n_basis = conv_conf["n_basis"]

    for problem_type in conv_conf["rhs"]:
        print(f"At problem type {problem_type}")
        problem_path = os.path.join(topdir, problem_type)
        if not os.path.exists(problem_path):
            os.mkdir(problem_path)
        PDE = locate("problems.poisson_rhs."+problem_type)
        errors_fd = []
        errors_dd = []
        errors_operator = []
        errors_rb = []

        for N in Ns:
            problem = PDE(config["problem"]["lx"], config["problem"]["ly"], config["problem"]["split_percentage"], N, N)
            fd_sol = problem.get_fd_solution()
            dd_sol, conv_dd = problem.get_dd_solution(maxiter, tolerance, miniter)
            operator_sol, conv_operator = problem.get_operator_solution(maxiter, tolerance, miniter)
            rb_sol, conv_rb = problem.get_rb_solution(n_basis, maxiter, tolerance, miniter, basis_type=basis_type)
            exact_sol = problem.get_exact_solution()
            correction_factor = np.sqrt(problem.hx * problem.hy)
            errors_fd.append(np.linalg.norm(fd_sol.flatten() - exact_sol.flatten()) * correction_factor)
            errors_dd.append(np.linalg.norm(
                dd_sol[:problem.nx1 + 1, :].flatten() - exact_sol[:problem.nx1 + 1, :].flatten()) * correction_factor)
            errors_operator.append(
                np.linalg.norm(operator_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten()) * correction_factor)
            errors_rb.append(
                np.linalg.norm(rb_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten()) * correction_factor)

        assert np.allclose(errors_dd, errors_operator)

        plt.figure()
        plt.loglog(Ns, errors_fd, label="Finite Difference", linestyle="-.", marker=".")
        plt.loglog(Ns, errors_dd, label="Domain Decomposition", linestyle="-", marker="x")
        plt.loglog(Ns, errors_operator, label="Exact Poincaré-Steklov", linestyle="-.", marker="o")
        plt.loglog(Ns, errors_rb, label="Reduced Basis", linestyle="-.", marker=".")
        plt.loglog(Ns, 1 / Ns ** 2, label="Reference quadratic")
        plt.xlabel("$N$ (number of disretisation points)")
        plt.ylabel("$L^2$-error")
        plt.grid()
        plt.legend()
        plt.title("$L^2$-error of numerical methods")
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "L^2-error of numerical methods.pdf"))

        plt.figure()
        plt.semilogy(conv_dd, linestyle="-.", marker=".")
        plt.title("Cauchy $L^2$-error of Domain Decomposition")
        plt.xlabel("Number of iterations")
        plt.ylabel("$L^2$-error")
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Cauchy L^2-error of Domain Decomposition.pdf"))

        plt.figure()
        plt.semilogy(conv_operator, linestyle="-.", marker=".")
        plt.title("Cauchy $L^2$-error of Exact Poincaré-Steklov")
        plt.xlabel("Number of iterations")
        plt.ylabel("$L^2$-error")
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Cauchy L^2-error of Exact Poincaré-Steklov.pdf"))

        plt.figure()
        plt.semilogy(conv_rb, linestyle="-.", marker=".")
        plt.title("Cauchy $L^2$-error of Reduced Basis")
        plt.xlabel("Number of iterations")
        plt.ylabel("$L^2$-error")
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Cauchy L^2-error of Reduced Basis.pdf"))

if "reduction" in config and config["reduction"]["run"]:
    red_conf = config["reduction"]
    reduction_type = red_conf["type"]
    print("Executing reduction error diagnostic report...")
    print("Using config: ")
    pprint.pprint(red_conf)

    Ns = np.array(red_conf["N"])
    basis_type = red_conf["basis_type"]
    dimensions = np.arange(red_conf["start_dimension"], red_conf["end_dimension"], red_conf["step_dimension"])

    for problem_type in red_conf["rhs"]:
        print(f"At problem type {problem_type}")
        problem_path = os.path.join(topdir, problem_type)
        if not os.path.exists(problem_path):
            os.mkdir(problem_path)
        PDE = locate("problems.poisson_rhs."+problem_type)

        errors_reduced = np.zeros((len(Ns), len(dimensions)))
        errors_exact = np.zeros((len(Ns), len(dimensions)))
        convs_rb = []

        for i, N in enumerate(Ns):
            print(f"Working on grid size {N}, which is {i + 1}/{len(Ns)}")
            for j, dim in enumerate(dimensions):
                problem = PDE(config["problem"]["lx"], config["problem"]["ly"], config["problem"]["split_percentage"], N, N)
                dd_sol, conv_dd = problem.get_dd_solution(maxiter, tolerance, miniter)
                if reduction_type == "whole":
                    rb_sol, conv_rb = problem.get_rb_solution(dim, maxiter, tolerance, miniter, basis_type=basis_type)
                elif reduction_type == "boundary":
                    rb_sol, conv_rb = problem.get_rb_solution_boundary(dim, maxiter, tolerance, miniter, basis_type=basis_type)
                else:
                    raise ValueError("Either 'whole' or 'boundary' reduction type")
                exact_sol = problem.get_exact_solution()
                correction_factor = np.sqrt(problem.hx * problem.hy)
                errors_reduced[i, j] = np.linalg.norm(
                    rb_sol.flatten() - dd_sol[:problem.nx1 + 1, :].flatten()) * correction_factor
                errors_exact[i, j] = np.linalg.norm(
                    rb_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten()) * correction_factor
                convs_rb.append(conv_rb)

        plt.figure()
        for i, N in enumerate(Ns):
            plt.semilogy(dimensions, errors_reduced[i, :], linestyle="-.", marker=".", label=f"Grid size {N}")
        plt.title("Reduced Error per number of Dimensions")
        plt.xticks(dimensions[::2])
        plt.xlabel("Number of dimensions")
        plt.ylabel("$L^2$-error")
        plt.legend()
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Reduced Error per number of Dimensions.pdf"))

        plt.figure()
        for i, N in enumerate(Ns):
            plt.semilogy(dimensions, errors_exact[i, :], linestyle="-.", marker=".", label=f"Grid size {N}")
        plt.title("Error when compared to exact solution")
        plt.xticks(dimensions[::2])
        plt.xlabel("Number of dimensions")
        plt.ylabel("$L^2$-error")
        plt.legend()
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Exact Reduced Error per number of Dimensions.pdf"))

        plt.figure()
        for conv, dim in zip(convs_rb[-len(Ns):], dimensions):
            plt.semilogy(conv, linestyle="-.", marker=".", label=f"Dimension {dim}")
        plt.title("Cauchy $L^2$ error of Reduced Basis")
        plt.xlabel("Number of iterations")
        plt.ylabel("$L^2$-error")
        plt.legend()
        plt.grid()
        plt.tight_layout()
        plt.savefig(os.path.join(problem_path, "Cauchy L^2 error of Reduced Basis.pdf"))
