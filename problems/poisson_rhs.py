from problems.poisson import Poisson2DAbstract
import numpy as np
import sympy as sp


class Poisson2DSine(Poisson2DAbstract):
    def __init__(self, *args, n_exact=2, m_exact=3, **kwargs):
        self.n_exact = n_exact
        self.m_exact = m_exact
        super().__init__(*args, **kwargs)

    def f(self, x, y):
        return ((self.n_exact * np.pi / self.lx) ** 2 + (self.m_exact * np.pi / self.ly) ** 2) \
               * np.sin(self.n_exact * np.pi / self.lx * x) \
               * np.sin(self.m_exact * np.pi / self.ly * y)

    def exact_solution_coordinate_wise(self, x, y):
        return np.sin(self.n_exact * np.pi / self.lx * x) * np.sin(self.m_exact * np.pi / self.ly * y)


def make_custom_poisson_factory_static(rhs, sol=lambda x, y: None):
    class Poisson2DAugmented(Poisson2DAbstract):
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

        def f(self, x, y):
            return rhs(x, y)

        def exact_solution_coordinate_wise(self, x, y):
            return sol(x, y)
    return Poisson2DAugmented


Poisson2DPolynomial = make_custom_poisson_factory_static(lambda x, y: -2 * y * (y - 1) - 2 * x * (x - 1),
                                                         lambda x, y: x * (x - 1) * y * (y - 1))


def calculate_gaussian_sol(p=0):
    x, y = sp.symbols("x y")
    gaussian_sol_expr = sp.sympify(f"x*(x-1)*y*(y-1)*exp(-(x-{p})^2-(y-{p})^2)", locals={"x": x, "y": y})
    gaussian_sol = sp.lambdify([x, y], gaussian_sol_expr, "numpy")
    gaussian_rhs = sp.lambdify([x, y], -sp.diff(sp.diff(gaussian_sol_expr, x), x) - sp.diff(sp.diff(gaussian_sol_expr, y), y), "numpy")
    return gaussian_rhs, gaussian_sol


Poisson2DGaussian = make_custom_poisson_factory_static(*calculate_gaussian_sol())


class Poisson2DGaussianWithParameter(Poisson2DAbstract):
    def __init__(self, *args, p, **kwargs):
        self.p = p
        self.gaussian_rhs, self.gaussian_sol = calculate_gaussian_sol(p)
        super().__init__(*args, **kwargs)

    def f(self, x, y):
        return self.gaussian_rhs(x, y)

    def exact_solution_coordinate_wise(self, x, y):
        return self.gaussian_sol(x, y)


