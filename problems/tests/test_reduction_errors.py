from problems.poisson_rhs import Poisson2DSine
import matplotlib.pyplot as plt
import numpy as np

Ns = [20, 40, 60]
dimensions = np.arange(2, 40+1, 2)
maxiter = 1000
miniter = 1
tolerance = 1e-8

errors_reduced = np.zeros((len(Ns), len(dimensions)))
errors_exact = np.zeros((len(Ns), len(dimensions)))
convs_rb = []

for i, N in enumerate(Ns):
    print(f"Working on grid size {N}, which is {i+1}/{len(Ns)}")
    for j, dim in enumerate(dimensions):
        problem = Poisson2DSine(1, 1, 0.6, N, N)
        dd_sol, conv_dd = problem.get_dd_solution(maxiter, tolerance, miniter)
        rb_sol, conv_rb = problem.get_rb_solution(dim, maxiter, tolerance, miniter, basis_type="fourier")
        exact_sol = problem.get_exact_solution()
        correction_factor = np.sqrt(problem.hx*problem.hy)
        errors_reduced[i, j] = np.linalg.norm(rb_sol.flatten() - dd_sol[:problem.nx1+1, :].flatten())*correction_factor
        errors_exact[i, j] = np.linalg.norm(rb_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten()) * correction_factor
        convs_rb.append(conv_rb)


plt.figure()
for i, N in enumerate(Ns):
    plt.semilogy(dimensions, errors_reduced[i, :], linestyle="-.", marker=".", label=f"Grid size {N}")
plt.title("Reduced Error per number of Dimensions")
plt.xlabel("Number of dimensions")
plt.ylabel("$L^2$-error")
plt.legend()
plt.grid()
plt.tight_layout()

plt.figure()
for i, N in enumerate(Ns):
    plt.semilogy(dimensions, errors_exact[i, :], linestyle="-.", marker=".", label=f"Grid size {N}")
plt.title("Error when compared to exact solution")
plt.xlabel("Number of dimensions")
plt.ylabel("$L^2$-error")
plt.grid()
plt.tight_layout()

plt.figure()
for conv, dim in zip(convs_rb[-len(Ns):], dimensions):
    plt.semilogy(conv, linestyle="-.", marker=".", label=f"Dimension {dim}")
plt.title("Cauchy $L^2$ error of Reduced Basis")
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.legend()
plt.grid()
plt.show()
