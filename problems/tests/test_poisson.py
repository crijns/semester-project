from problems.poisson_rhs import Poisson2DSine
from utils.helper_functions import plot
import matplotlib.pyplot as plt
from numpy.linalg import norm

maxiter = 1000
miniter = 1
tolerance = 1e-8
clims = [-1, 1]

problem = Poisson2DSine(1, 1, 0.6, 40, 40)

u_exact_sol = problem.get_exact_solution()
u_fd_sol = problem.get_fd_solution()
u_dd_sol, errors_dd = problem.get_dd_solution(maxiter, tolerance, miniter)
u_operator_sol, errors_operator = problem.get_operator_solution(maxiter, tolerance, miniter)
u_rb_sol, errors_rb = problem.get_rb_solution_boundary(5, maxiter, tolerance, miniter, basis_type="fourier")

print("FD vs DD error:", norm((u_fd_sol[:problem.nx1+1, :] - u_dd_sol).flatten())*problem.hy)
print("DD vs RB error:", norm((u_dd_sol-u_rb_sol).flatten())*problem.hy)

plt.figure()
plot(u_exact_sol, [0, 1, 0, 1])
plt.clim(*clims)
plt.title("Exact Solution")

plt.figure()
plot(u_fd_sol, [0, 1, 0, 1])
plt.clim(*clims)
plt.title("Finite Difference Solution")

plt.figure()
plt.suptitle("Domain Decomposition Solution")
plt.subplot(121)
plot(u_dd_sol, [0, 0.6, 0, 1])
plt.clim(*clims)
plt.subplot(122)
plt.semilogy(errors_dd)
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.tight_layout()

plt.figure()
plt.suptitle("Operator Solution")
plt.subplot(121)
plot(u_operator_sol, [0, 0.6, 0, 1])
plt.clim(*clims)
plt.subplot(122)
plt.semilogy(errors_operator)
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.tight_layout()

plt.figure()
plt.suptitle("Reduced Basis Solution")
plt.subplot(121)
plot(u_rb_sol, [0, 0.6, 0, 1])
plt.clim(*clims)
plt.subplot(122)
plt.semilogy(errors_rb)
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.tight_layout()

plt.figure()
plt.plot(u_rb_sol[:, 13], label="rb")
plt.plot(u_dd_sol[:, 13], label="dd")
plt.legend()

plt.show()
