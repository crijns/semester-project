from problems.poisson_rhs import Poisson2DPolynomial
import matplotlib.pyplot as plt
import numpy as np

#Ns = np.array([10, 20, 40, 80, 160, 320])

Ns = np.array([10, 20, 40, 80])
maxiter = 1000
miniter = 400
tolerance = 1e-5

errors_fd = []
errors_dd = []
errors_operator = []
errors_rb = []


for N in Ns:
    problem = Poisson2DPolynomial(1, 1, 0.6, N, N)
    fd_sol = problem.get_fd_solution()
    dd_sol, conv_dd = problem.get_dd_solution(maxiter, tolerance, miniter)
    operator_sol, conv_operator = problem.get_operator_solution(maxiter, tolerance, miniter)
    rb_sol, conv_rb = problem.get_rb_solution(5, maxiter, tolerance, miniter)
    exact_sol = problem.get_exact_solution()
    correction_factor = np.sqrt(problem.hx*problem.hy)
    errors_fd.append(np.linalg.norm(fd_sol.flatten()-exact_sol.flatten())*correction_factor)
    errors_dd.append(np.linalg.norm(dd_sol[:problem.nx1+1, :].flatten()-exact_sol[:problem.nx1+1, :].flatten())*correction_factor)
    errors_operator.append(np.linalg.norm(operator_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten())*correction_factor)
    errors_rb.append(np.linalg.norm(rb_sol.flatten() - exact_sol[:problem.nx1 + 1, :].flatten())*correction_factor)

assert np.allclose(errors_dd, errors_operator)
np.savez("../../bug.npz", Ns=Ns, errors_fd=errors_fd, errors_dd=errors_dd, errors_operator=errors_operator, errors_rb=errors_rb)

plt.loglog(Ns, errors_fd, label="Finite Difference", linestyle="-.", marker=".")
plt.loglog(Ns, errors_dd, label="Domain Decomposition", linestyle="-", marker="x")
plt.loglog(Ns, errors_operator, label="Exact Poincaré-Steklov", linestyle="-.", marker=".")
plt.loglog(Ns, errors_rb, label="Reduced Basis", linestyle="-.", marker=".")
plt.loglog(Ns, 1/Ns**2, label="Reference quadratic")
plt.xlabel("$N$ (number of disretisation points)")
plt.ylabel("$L^2$-error")
plt.grid()
plt.legend()
plt.title("$L^2$-error of numerical methods")

plt.figure()
plt.semilogy(conv_dd, linestyle="-.", marker=".")
plt.title("Cauchy $L^2$-error of Domain Decomposition")
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.grid()

plt.figure()
plt.semilogy(conv_operator, linestyle="-.", marker=".")
plt.title("Cauchy $L^2$-error of Exact Poincaré-Steklov")
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.grid()

plt.figure()
plt.semilogy(conv_rb, linestyle="-.", marker=".")
plt.title("Cauchy $L^2$-error of Reduced Basis")
plt.xlabel("Number of iterations")
plt.ylabel("$L^2$-error")
plt.grid()

plt.show()
