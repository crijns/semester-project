# Semester Project Lucas Crijns

This semester project is under supervision of Niccolò Discacciati and prof. J.S. Hesthaven. 

This repository holds all code related to my semester project.

## Dependencies
This project makes use of Python 3.10.
To install dependencies needed for this project, run:
```
$ pip install -r requirements.txt
```

Another option is to run it in a container:
```
$ docker build -t semester .
$ docker run -it semester python <file>
```
The `docker` command can be substituted by `podman`.

## What does each file do?
The following packages are in here:
* `demonstration`: Proof of concept
  * `poincare_steklov.py`: the proof of concept
* `problems`: Rewrite of demonstration with more flexibility
  * `poisson.py` and `poisson_other_rhs.py` contain the Poisson PDE with different solutions
  * `test_poisson.py`: Runs basic testing of the functions in `poisson.py`
  * `test_convergence.py`: Plots the convergence of methods
  * `test_reduction_errors.py`: Plots the reduction errors ($L^2$ error of DD and RB)as a function of basis size
* `scenarios`: Here are the neural network search and tryouts
  * `test_neural_network_on_poisson.py`: Tests running a basic neural network on the Poisson PDE
  * `find_optimal_network`: Performs a hyperparameter search to find the optimal network.
* `neural_nets`: Basic model building functions
* `utils`: Basic helper functions
