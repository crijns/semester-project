from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Normalization


def build_FNN(n_output, activation, n_units, length, regularizer=None):
    components = [Normalization()]  # Dense(units=n_units, activation=activation, kernel_regularizer=regularizer())]
    components += [Dense(units=n_units, activation=activation, kernel_regularizer=regularizer()) for _ in range(length-2)]
    components += [Dense(units=n_output, activation='linear', kernel_regularizer=regularizer())]
    return Sequential(layers=components)

